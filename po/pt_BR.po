# Brazilian Portuguese translation of pavucontrol.
# This file is distributed under the same license as the pavucontrol package.
# Copyright (C) 2008 Free Software Foundation, Inc.
#
# Jyulliano Arruda Ferraro Rocha <jyulliano@gmail.com>, 2008.
# Igor Pires Soares <igor@projetofedora.org>, 2008,2009.
# Henrique Roberto Gattermann Mittelstaedt <henrique.roberto97@gmail.com>, 2020.
# Rafael Fontenelle <rafaelff@gnome.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: pavucontrol\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-17 00:01+0300\n"
"PO-Revision-Date: 2021-09-21 06:04+0000\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Portuguese (Brazil) <https://translate.fedoraproject.org/"
"projects/pulseaudio/pavucontrol/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.8\n"
"X-Poedit-Language: Portuguese\n"
"X-Poedit-Country: BRAZIL\n"

#: src/pavucontrol.desktop.in:4 src/pavucontrol.cc:874
msgid "PulseAudio Volume Control"
msgstr "Controle de volume do PulseAudio"

#: src/pavucontrol.desktop.in:5 src/pavucontrol.glade:785
msgid "Volume Control"
msgstr "Controle de volume"

#: src/pavucontrol.desktop.in:6
msgid "Adjust the volume level"
msgstr "Ajuste o nível do volume"

#: src/pavucontrol.desktop.in:8
#, fuzzy
msgid "multimedia-volume-control"
msgstr "Controle de volume do PulseAudio"

#: src/pavucontrol.desktop.in:12
msgid ""
"pavucontrol;Microphone;Volume;Fade;Balance;Headset;Speakers;Headphones;Audio;"
"Mixer;Output;Input;Devices;Playback;Recording;System Sounds;Sound Card;"
"Settings;Preferences;"
msgstr ""
"pavucontrol;Microphone;Volume;Fade;Balance;Headset;Speakers;Headphones;Audio;"
"Mixer;Output;Input;Devices;Playback;Recording;System Sounds;Sound Card;"
"Settings;Preferences;microfone;equilíbrio;áudio;saída;entrada;dispositivos;"
"reprodução;gravação;sistemas de som;placa de som;configurações;preferências;"

#: src/pavucontrol.glade:12
msgid "window2"
msgstr ""

#: src/pavucontrol.glade:25
msgid "<b>left-front</b>"
msgstr "<b>frontal-esquerdo</b>"

#: src/pavucontrol.glade:55
msgid "<small>50%</small>"
msgstr "<small>50%</small>"

#: src/pavucontrol.glade:74 src/pavucontrol.glade:279
#: src/pavucontrol.glade:1513
msgid "window1"
msgstr ""

#: src/pavucontrol.glade:119
msgid "Card Name"
msgstr "Nome da placa"

#: src/pavucontrol.glade:147
msgid "Lock card to this profile"
msgstr "Travar placa para este perfil"

#: src/pavucontrol.glade:188
msgid "<b>Profile:</b>"
msgstr "<b>Perfil:</b>"

#: src/pavucontrol.glade:218
msgid "<b>Codec:</b>"
msgstr "<b>Codec:</b>"

#: src/pavucontrol.glade:335
msgid "Device Title"
msgstr "Título do Dispositivo"

#: src/pavucontrol.glade:363 src/pavucontrol.glade:1625
msgid "Mute audio"
msgstr "Sem áudio"

#: src/pavucontrol.glade:385 src/pavucontrol.glade:1647
msgid "Lock channels together"
msgstr "Travar canais juntos"

#: src/pavucontrol.glade:408
msgid "Set as fallback"
msgstr "Definir como secundário"

#: src/pavucontrol.glade:446
msgid "<b>Port:</b>"
msgstr "<b>Porta:</b>"

#: src/pavucontrol.glade:506
msgid "PCM"
msgstr "PCM"

#: src/pavucontrol.glade:521
msgid "AC-3"
msgstr "AC-3"

#: src/pavucontrol.glade:534
msgid "DTS"
msgstr "DTS"

#: src/pavucontrol.glade:547
msgid "E-AC-3"
msgstr "E-AC-3"

#: src/pavucontrol.glade:560
msgid "MPEG"
msgstr "MPEG"

#: src/pavucontrol.glade:573
msgid "AAC"
msgstr "AAC"

#: src/pavucontrol.glade:586
msgid "TrueHD"
msgstr ""

#: src/pavucontrol.glade:599
msgid "DTS-HD"
msgstr ""

#: src/pavucontrol.glade:626
msgid "<b>Latency offset:</b>"
msgstr "<b>Compensação de latência:</b>"

#: src/pavucontrol.glade:654
msgid "ms"
msgstr "ms"

#: src/pavucontrol.glade:676
msgid "Advanced"
msgstr "Avançado"

#: src/pavucontrol.glade:716 src/pavucontrol.glade:733
msgid "All Streams"
msgstr "Todos os fluxos"

#: src/pavucontrol.glade:719 src/pavucontrol.glade:736
msgid "Applications"
msgstr "Aplicações"

#: src/pavucontrol.glade:722 src/pavucontrol.glade:739
msgid "Virtual Streams"
msgstr "Fluxos virtuais"

#: src/pavucontrol.glade:750
msgid "All Output Devices"
msgstr "Todos os Dispositivos de Saída"

#: src/pavucontrol.glade:753
msgid "Hardware Output Devices"
msgstr "Dispositivos de saída de hardware"

#: src/pavucontrol.glade:756
msgid "Virtual Output Devices"
msgstr "Dispositivos de saída virtuais"

#: src/pavucontrol.glade:767
msgid "All Input Devices"
msgstr "Todos os Dispositivos de Entrada"

#: src/pavucontrol.glade:770
msgid "All Except Monitors"
msgstr "Todos exceto monitores"

#: src/pavucontrol.glade:773
msgid "Hardware Input Devices"
msgstr "Dispositivos de entrada de hardware"

#: src/pavucontrol.glade:776
msgid "Virtual Input Devices"
msgstr "Dispositivos de entrada virtuais"

#: src/pavucontrol.glade:779
msgid "Monitors"
msgstr "Monitores"

#: src/pavucontrol.glade:828
msgid "<i>No application is currently playing audio.</i>"
msgstr "<i>Nenhum aplicativo está reproduzindo áudio no momento.</i>"

#: src/pavucontrol.glade:876 src/pavucontrol.glade:1000
msgid "<b>_Show:</b>"
msgstr "<b>_Mostrar:</b>"

#: src/pavucontrol.glade:920
msgid "_Playback"
msgstr "_Reprodução"

#: src/pavucontrol.glade:952
msgid "<i>No application is currently recording audio.</i>"
msgstr "<i>Nenhum aplicativo está gravando áudio no momento.</i>"

#: src/pavucontrol.glade:1047
msgid "_Recording"
msgstr "_Gravando"

#: src/pavucontrol.glade:1080
msgid "<i>No output devices available</i>"
msgstr "<i>Nenhum dispositivo de saída disponível</i>"

#: src/pavucontrol.glade:1128
msgid "<b>S_how:</b>"
msgstr "<b>M_ostrar:</b>"

#: src/pavucontrol.glade:1176
msgid "_Output Devices"
msgstr "Dispositivos de _saída"

#: src/pavucontrol.glade:1210
msgid "<i>No input devices available</i>"
msgstr "<i>Nenhum dispositivo de entrada disponível</i>"

#: src/pavucontrol.glade:1258
msgid "<b>Sho_w:</b>"
msgstr "<b>Mos_trar:</b>"

#: src/pavucontrol.glade:1306
msgid "_Input Devices"
msgstr "Dispositivos de _entrada"

#: src/pavucontrol.glade:1343
msgid "<i>No cards available for configuration</i>"
msgstr "<i>Nenhuma placa disponível para configuração</i>"

#: src/pavucontrol.glade:1376
msgid "Show volume meters"
msgstr "Mostrar medidores de volume"

#: src/pavucontrol.glade:1416
msgid "_Configuration"
msgstr "_Configuração"

#: src/pavucontrol.glade:1481
msgid "<b>Rename device to:</b>"
msgstr "<b>Renomear dispositivo para:</b>"

#: src/pavucontrol.glade:1570
msgid "Stream Title"
msgstr "Título do fluxo"

#: src/pavucontrol.glade:1586
msgid "direction"
msgstr "direção"

#: src/pavucontrol.cc:104
#, c-format
msgid "could not read JSON from list-codecs message response: %s"
msgstr "não foi possível ler JSON da resposta de mensagem de list-codecs: %s"

#: src/pavucontrol.cc:113
msgid "list-codecs message response is not a JSON array"
msgstr "resposta de mensagem de list-codecs não é um vetor JSON"

#: src/pavucontrol.cc:161
msgid "list-codecs message response could not be parsed correctly"
msgstr ""

#: src/pavucontrol.cc:181
#, c-format
msgid "could not read JSON from get-codec message response: %s"
msgstr ""

#: src/pavucontrol.cc:190
msgid "get-codec message response is not a JSON value"
msgstr ""

#: src/pavucontrol.cc:198
msgid "could not get codec name from get-codec message response"
msgstr ""

#: src/pavucontrol.cc:220
#, c-format
msgid "could not read JSON from get-profile-sticky message response: %s"
msgstr ""

#: src/pavucontrol.cc:229
msgid "get-profile-sticky message response is not a JSON value"
msgstr ""

#: src/pavucontrol.cc:249 src/cardwidget.cc:153 src/cardwidget.cc:181
#, fuzzy, c-format
msgid "pa_context_send_message_to_object() failed: %s"
msgstr "pa_context_set_default_source() falhou"

#: src/pavucontrol.cc:267
#, c-format
msgid "could not read JSON from list-handlers message response: %s"
msgstr ""

#: src/pavucontrol.cc:276
msgid "list-handlers message response is not a JSON array"
msgstr ""

#: src/pavucontrol.cc:324
msgid "list-handlers message response could not be parsed correctly"
msgstr ""

#: src/pavucontrol.cc:358
msgid "Card callback failure"
msgstr "Falha na chamada de retorno da placa"

#: src/pavucontrol.cc:386
msgid "Sink callback failure"
msgstr "Falha no destino da chamada de retorno"

#: src/pavucontrol.cc:410
msgid "Source callback failure"
msgstr "Chamada da fonte de retorno falhou"

#: src/pavucontrol.cc:429
msgid "Sink input callback failure"
msgstr "Falha no destino da entrada das chamadas de retorno"

#: src/pavucontrol.cc:448
msgid "Source output callback failure"
msgstr "Falha na chamada de retorno da saída da fonte"

#: src/pavucontrol.cc:478
msgid "Client callback failure"
msgstr "Falha na chamada de retorno do cliente"

#: src/pavucontrol.cc:494
msgid "Server info callback failure"
msgstr "Falha na chamada de retorno das informações do servidor"

#: src/pavucontrol.cc:512 src/pavucontrol.cc:809
#, c-format
msgid "Failed to initialize stream_restore extension: %s"
msgstr "Falha ao inicializar a extensão stream_restore: %s"

#: src/pavucontrol.cc:530
msgid "pa_ext_stream_restore_read() failed"
msgstr "pa_ext_stream_restore_read() falhou"

#: src/pavucontrol.cc:548 src/pavucontrol.cc:823
#, c-format
msgid "Failed to initialize device restore extension: %s"
msgstr "Falha ao inicializar a extensão de restauração de dispositivo: %s"

#: src/pavucontrol.cc:569
msgid "pa_ext_device_restore_read_sink_formats() failed"
msgstr "pa_ext_device_restore_read_sink_formats() falhou"

#: src/pavucontrol.cc:587 src/pavucontrol.cc:836
#, c-format
msgid "Failed to initialize device manager extension: %s"
msgstr "Falha ao inicializar a extensão de gerência de dispositivo: %s"

#: src/pavucontrol.cc:606
msgid "pa_ext_device_manager_read() failed"
msgstr "pa_ext_device_manager_read() falhou"

#: src/pavucontrol.cc:623
msgid "pa_context_get_sink_info_by_index() failed"
msgstr "pa_context_get_sink_info_by_index() falhou"

#: src/pavucontrol.cc:636
msgid "pa_context_get_source_info_by_index() failed"
msgstr "pa_context_get_source_info_by_index() falhou"

#: src/pavucontrol.cc:649 src/pavucontrol.cc:662
msgid "pa_context_get_sink_input_info() failed"
msgstr "pa_context_get_sink_input_info() falhou"

#: src/pavucontrol.cc:675
msgid "pa_context_get_client_info() failed"
msgstr "pa_context_get_client_info() falhou"

#: src/pavucontrol.cc:685 src/pavucontrol.cc:750
msgid "pa_context_get_server_info() failed"
msgstr "pa_context_get_server_info() falhou"

#: src/pavucontrol.cc:698
msgid "pa_context_get_card_info_by_index() failed"
msgstr "pa_context_get_card_info_by_index() falhou"

#: src/pavucontrol.cc:741
msgid "pa_context_subscribe() failed"
msgstr "pa_context_subscribe() falhou"

#: src/pavucontrol.cc:757
msgid "pa_context_client_info_list() failed"
msgstr "pa_context_client_info_list() falhou"

#: src/pavucontrol.cc:764
msgid "pa_context_get_card_info_list() failed"
msgstr "pa_context_get_card_info_list() falhou"

#: src/pavucontrol.cc:771
msgid "pa_context_get_sink_info_list() failed"
msgstr "pa_context_get_sink_info_list() falhou"

#: src/pavucontrol.cc:778
msgid "pa_context_get_source_info_list() failed"
msgstr "pa_context_get_source_info_list() falhou"

#: src/pavucontrol.cc:785
msgid "pa_context_get_sink_input_info_list() failed"
msgstr "pa_context_get_sink_input_info_list() falhou"

#: src/pavucontrol.cc:792
msgid "pa_context_get_source_output_info_list() failed"
msgstr "pa_context_get_source_output_info_list() falhou"

#: src/pavucontrol.cc:851 src/pavucontrol.cc:902
msgid "Connection failed, attempting reconnect"
msgstr "Falha na conexão, tentando reconectar"

#: src/pavucontrol.cc:889
msgid ""
"Connection to PulseAudio failed. Automatic retry in 5s\n"
"\n"
"In this case this is likely because PULSE_SERVER in the Environment/X11 Root "
"Window Properties\n"
"or default-server in client.conf is misconfigured.\n"
"This situation can also arise when PulseAudio crashed and left stale details "
"in the X11 Root Window.\n"
"If this is the case, then PulseAudio should autospawn again, or if this is "
"not configured you should\n"
"run start-pulseaudio-x11 manually."
msgstr ""
"A conexão com o PulseAudio falhou. Nova tentativa automática em 5s\n"
"\n"
"Neste caso, é provável que PULSE_SERVER em Environment/X11 Root Window "
"Properties\n"
"ou default-server em client.conf estejam configurados incorretamente.\n"
"Isso também pode acontecer quando o PulseAudio travou e deixou detalhes "
"travados na X11 Root Window.\n"
"Se este for o caso, então o PulseAudio deve ser reiniciar automaticamente de "
"novo ou, se não estiver\n"
"configurado, você deve executar start-pulseaudio-x11 manualmente."

#: src/cardwidget.cc:126
msgid "pa_context_set_card_profile_by_index() failed"
msgstr "pa_context_set_card_profile_by_index() falhou"

#: src/channelwidget.cc:101
#, c-format
msgid "<small>%0.0f%% (%0.2f dB)</small>"
msgstr "<small>%0.0f%% (%0.2f dB)</small>"

#: src/channelwidget.cc:103
#, c-format
msgid "<small>%0.0f%% (-&#8734; dB)</small>"
msgstr "<small>%0.0f%% (-&#8734; dB)</small>"

#: src/channelwidget.cc:106
#, c-format
msgid "%0.0f%%"
msgstr "%0.0f%%"

#: src/channelwidget.cc:139
msgid "<small>Silence</small>"
msgstr "<small>Silencioso</small>"

#: src/channelwidget.cc:139
msgid "<small>Min</small>"
msgstr "<small>Min</small>"

#: src/channelwidget.cc:141
msgid "<small>100% (0 dB)</small>"
msgstr "<small>100% (0 dB)</small>"

#: src/channelwidget.cc:145
msgid "<small><i>Base</i></small>"
msgstr "<small><i>Básico</i></small>"

#: src/devicewidget.cc:59
msgid "Rename Device..."
msgstr "Renomear Dispositivo..."

#: src/devicewidget.cc:163
msgid "pa_context_set_port_latency_offset() failed"
msgstr "pa_context_set_port_latency_offset() falhou"

#: src/devicewidget.cc:244
msgid "Sorry, but device renaming is not supported."
msgstr "Desculpe, mas a renomeação de dispositivo não é suportada."

#: src/devicewidget.cc:249
msgid ""
"You need to load module-device-manager in the PulseAudio server in order to "
"rename devices"
msgstr ""
"Você precisa carregar o module-device-manager no servidor PulseAudio para "
"renomear os dispositivos"

#: src/devicewidget.cc:262
msgid "_Cancel"
msgstr "_Cancelar"

#: src/devicewidget.cc:263
msgid "_OK"
msgstr "_OK"

#: src/devicewidget.cc:270
msgid "pa_ext_device_manager_write() failed"
msgstr "pa_ext_device_manager_write() falhou"

#: src/mainwindow.cc:171
#, c-format
msgid "Error reading config file %s: %s"
msgstr "Erro lendo o arquivo de configuração %s: %s"

#: src/mainwindow.cc:250
msgid "Error saving preferences"
msgstr "Erro salvando as preferências"

#: src/mainwindow.cc:258
#, c-format
msgid "Error writing config file %s"
msgstr "Erro escrevendo o arquivo de configuração %s"

#: src/mainwindow.cc:322
msgid " (plugged in)"
msgstr " (conectado)"

#: src/mainwindow.cc:326 src/mainwindow.cc:434
msgid " (unavailable)"
msgstr " (indisponível)"

#: src/mainwindow.cc:328 src/mainwindow.cc:431
msgid " (unplugged)"
msgstr " (desconectado)"

#: src/mainwindow.cc:633
msgid "Failed to read data from stream"
msgstr "Falha ao ler dados procedentes do fluxo"

#: src/mainwindow.cc:677
msgid "Peak detect"
msgstr "Detectar pico"

#: src/mainwindow.cc:678
msgid "Failed to create monitoring stream"
msgstr "Falha ao criar o fluxo de monitoramento"

#: src/mainwindow.cc:693
msgid "Failed to connect monitoring stream"
msgstr "Falha ao conectar o fluxo de monitoramento"

#: src/mainwindow.cc:830
msgid ""
"Ignoring sink-input due to it being designated as an event and thus handled "
"by the Event widget"
msgstr ""
"Ignorando sink-input por ter sido designado como um evento e, portanto, "
"tratado pelo widget de evento"

#: src/mainwindow.cc:1005
msgid "System Sounds"
msgstr "Sons do sistema"

#: src/mainwindow.cc:1351
msgid "Establishing connection to PulseAudio. Please wait..."
msgstr "Estabelecendo conexão com o PulseAudio. Por favor, aguarde…"

#: src/rolewidget.cc:72
msgid "pa_ext_stream_restore_write() failed"
msgstr "pa_ext_stream_restore_write() falhou"

#: src/sinkinputwidget.cc:35
msgid "on"
msgstr "em"

#: src/sinkinputwidget.cc:38
msgid "Terminate Playback"
msgstr "Terminar reprodução"

#: src/sinkinputwidget.cc:78
msgid "Unknown output"
msgstr "Saída desconhecida"

#: src/sinkinputwidget.cc:87
msgid "pa_context_set_sink_input_volume() failed"
msgstr "pa_context_set_sink_input_volume() falhou"

#: src/sinkinputwidget.cc:102
msgid "pa_context_set_sink_input_mute() failed"
msgstr "pa_context_set_sink_input_mute() falhou"

#: src/sinkinputwidget.cc:112
msgid "pa_context_kill_sink_input() failed"
msgstr "pa_context_kill_sink_input() falhou"

#: src/sinkwidget.cc:117
msgid "pa_context_set_sink_volume_by_index() failed"
msgstr "pa_context_set_sink_volume_by_index() falhou"

#: src/sinkwidget.cc:132
msgid "Volume Control Feedback Sound"
msgstr "Controle do volume do som de retorno"

#: src/sinkwidget.cc:149
msgid "pa_context_set_sink_mute_by_index() failed"
msgstr "pa_context_set_sink_mute_by_index() falhou"

#: src/sinkwidget.cc:163
msgid "pa_context_set_default_sink() failed"
msgstr "pa_context_set_default_sink() falhou"

#: src/sinkwidget.cc:183
msgid "pa_context_set_sink_port_by_index() failed"
msgstr "pa_context_set_sink_port_by_index() falhou"

#: src/sinkwidget.cc:225
msgid "pa_ext_device_restore_save_sink_formats() failed"
msgstr "pa_ext_device_restore_save_sink_formats() falhou"

#: src/sourceoutputwidget.cc:35
msgid "from"
msgstr "de"

#: src/sourceoutputwidget.cc:38
msgid "Terminate Recording"
msgstr "Terminar gravação"

#: src/sourceoutputwidget.cc:83
msgid "Unknown input"
msgstr "Entrada desconhecida"

#: src/sourceoutputwidget.cc:93
msgid "pa_context_set_source_output_volume() failed"
msgstr "pa_context_set_source_output_volume() falhou"

#: src/sourceoutputwidget.cc:108
msgid "pa_context_set_source_output_mute() failed"
msgstr "pa_context_set_source_output_mute() falhou"

#: src/sourceoutputwidget.cc:119
msgid "pa_context_kill_source_output() failed"
msgstr "pa_context_kill_source_output() falhou"

#: src/sourcewidget.cc:46
msgid "pa_context_set_source_volume_by_index() failed"
msgstr "pa_context_set_source_volume_by_index() falhou"

#: src/sourcewidget.cc:61
msgid "pa_context_set_source_mute_by_index() failed"
msgstr "pa_context_set_source_mute_by_index() falhou"

#: src/sourcewidget.cc:75
msgid "pa_context_set_default_source() failed"
msgstr "pa_context_set_default_source() falhou"

#: src/sourcewidget.cc:97
msgid "pa_context_set_source_port_by_index() failed"
msgstr "pa_context_set_source_port_by_index() falhou"

#: src/streamwidget.cc:52
msgid "Terminate"
msgstr "Terminar"

#: src/pavuapplication.cc:160
msgid "Select a specific tab on load."
msgstr "Selecione uma aba específica para carregar."

#: src/pavuapplication.cc:161
msgid "number"
msgstr "número"

#: src/pavuapplication.cc:166
msgid "Retry forever if pa quits (every 5 seconds)."
msgstr "Tenta novamente sempre se PulseAudio for encerrado (todo 5 segundos)."

#: src/pavuapplication.cc:171
msgid "Maximize the window."
msgstr "Maximiza a janela."

#: src/pavuapplication.cc:176
msgid "Show version."
msgstr "Mostra a versão."

#~ msgid "50%"
#~ msgstr "50%"

#~ msgid ""
#~ "All Input Devices\n"
#~ "All Except Monitors\n"
#~ "Hardware Input Devices\n"
#~ "Virtual Input Devices\n"
#~ "Monitors"
#~ msgstr ""
#~ "Todos os dispositivos de entrada\n"
#~ "Todos exceto monitores\n"
#~ "Hardware de dispositivos de entrada\n"
#~ "Dispositivos virtuais de entrada\n"
#~ "Monitores"

#~ msgid ""
#~ "All Output Devices\n"
#~ "Hardware Output Devices\n"
#~ "Virtual Output Devices"
#~ msgstr ""
#~ "Todos os dispositivos de saída\n"
#~ "Hardware dos dispositivos de saída\n"
#~ "Dispositivos virtuais de saída"

#~ msgid ""
#~ "All Streams\n"
#~ "Applications\n"
#~ "Virtual Streams"
#~ msgstr ""
#~ "Todos os fluxos\n"
#~ "Aplicativos\n"
#~ "Fluxos virtuais"

#~ msgid "<small>Max</small>"
#~ msgstr "<small>Máximo</small>"

#~ msgid "pa_context_move_sink_input_by_index() failed"
#~ msgstr "pa_context_move_sink_input_by_index() falhou"

#~ msgid "pa_context_move_source_output_by_index() failed"
#~ msgstr "pa_context_move_source_output_by_index() falhou"

#~ msgid "Open menu"
#~ msgstr "Abrir menu"

#~ msgid "_Move Stream..."
#~ msgstr "_Mover fluxo..."

#~ msgid "_Default"
#~ msgstr "_Padrão"
